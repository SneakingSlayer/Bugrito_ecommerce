import React from 'react'
import Header from '../components/partials/Header'
import Register from '../components/Register'
import Footer from '../components/partials/Footer'
export default function RegisterPage() {
    return (
        <div>
            <Header/>
            <Register/>
            <Footer/>
        </div>
    )
}
