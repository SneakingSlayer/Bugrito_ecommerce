import React from 'react';
import Item from '../components/Item'
import Header from '../components/partials/Header'
import Footer from '../components/partials/Footer'
export default function ItemPage() {
    return (
        <div>
    
            <Item/>
            <Footer/>
            
        </div>
    )
}
