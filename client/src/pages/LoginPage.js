import React from 'react'
import Header from '../components/partials/Header'
import Login from '../components/Login'
import Footer from '../components/partials/Footer'
export default function LoginPage() {
    return (
        <>
        <Header/>
        <Login/>
        <Footer/>
        </>
    )
}
